FROM gradle:latest
WORKDIR /work
ADD ./build.gradle /work/build.gradle
ADD ./src  /work/src
ADD ./settings.gradle /work/settings.gradle
RUN gradle shadowJar && ls /work/build/libs/
EXPOSE 4567
CMD java -jar /work/build/libs/fustuLadioBot-all.jar
